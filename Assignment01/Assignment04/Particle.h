#pragma once

#include "precomp.h"

class Particle
{

  public:
	int m_X;
	int m_Y;
	Pixel m_Color;

	void Update( Surface *surface );
	void Draw( Surface *surface );

		Particle(  )
	{
		m_X = 0;
		m_Y = 0;
		m_Color = 0x00000000;
	}

	Particle( int x, int y, int color )
	{
		m_X = x;
		m_Y = y;
		m_Color = color;
	}

  private:
	Pixel *ComputePointerToPixel( Surface *surface );
};
