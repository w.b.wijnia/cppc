#include "RandomGens.h"
#include "precomp.h"

// -------------------------------------------------------------
// From the tutorial. this one can be considered 'reasonable well'.
// -------------------------------------------------------------

unsigned int xorShiftSeed = 123456;

unsigned int xorshift32()
{
	xorShiftSeed ^= xorShiftSeed << 13;
	xorShiftSeed ^= xorShiftSeed >> 17;
	xorShiftSeed ^= xorShiftSeed << 5;
	return xorShiftSeed;
}

unsigned int xorshift32( int min, int max )
{
	unsigned int r = xorshift32();
	return min + ( r % ( max - min ) );
}

unsigned int xorshift32( int numberOfBits )
{
	unsigned int r = xorshift32();
	unsigned int mask = ( 1 << numberOfBits ) - 1;
	return r & mask;
}

// ---------------------------------------------------------------------
// From various websites. These too can be considered 'reasonable well'.
// ---------------------------------------------------------------------

// https://en.wikipedia.org/wiki/Wichmann%E2%80%93Hill
// Whichman-Hill

unsigned int whSeed1 = 524287, whSeed2 = 131071, whSeed3 = 8191;

double whichmannHill()
{
	whSeed1 = ( 171 * whSeed1 ) % 30269;
	whSeed2 = ( 172 * whSeed2 ) % 30307;
	whSeed3 = ( 170 * whSeed3 ) % 30323;

	double integral;
	return modf( ( ( whSeed1 / 30269.0 ) + ( whSeed2 / 30307.0 ) + ( whSeed3 / 30323.0 ) ), &integral );
}

unsigned int whichmannHill32( int min, int max )
{
	unsigned int r = whichmannHill() * UINT_MAX;
	return r % ( max - min ) + min;
}

unsigned int whichmannHill32( int numberOfBits )
{
	unsigned int r = ( whichmannHill() * UINT_MAX );
	unsigned int mask = ( 1 << numberOfBits ) - 1;
	return r & mask;
}

// ---------------------------------------------------------------------
// A few good ones - some are quick and dirty, other's are exceptionally good.
// ---------------------------------------------------------------------

// https://groups.google.com/forum/#!msg/comp.lang.c/qZFQgKRCQGg/rmPkaRHqxOMJ

// 'quick and dirty', a period of  2^32.
unsigned int seedQAD = 1234;
unsigned int rng1()
{
	seedQAD = 69069 * seedQAD + 362437;
	return seedQAD;
}

unsigned int rng1( int min, int max )
{
	unsigned int r = rng1();
	return r % ( max - min ) + min;
}

unsigned int rng1( int numberOfBits )
{
	unsigned int r = ( rng1() * UINT_MAX );
	unsigned int mask = ( 1 << numberOfBits ) - 1;
	return r & mask;
}

// a period of 2^160.

unsigned int x = 123456789,
			 y = 362436069,
			 z = 521288629,
			 w = 88675123,
			 v = 886756453;

unsigned int rng2()
{
	unsigned long t;
	t = ( x ^ ( x >> 7 ) );
	x = y;
	y = z;
	z = w;
	w = v;
	v = ( v ^ ( v << 6 ) ) ^ ( t ^ ( t << 13 ) );
	return ( y + y + 1 ) * v;
}

unsigned int rng2( int min, int max )
{
	unsigned int r = rng2();
	return r % ( max - min ) + min;
}

unsigned int rng2( int numberOfBits )
{
	unsigned int r = ( rng2() * UINT_MAX );
	unsigned int mask = ( 1 << numberOfBits ) - 1;
	return r & mask;
}

// a period of about 2 ^ 8222

static unsigned int Q[256], c = 362436;

void InitializeRNG3()
{
	for (int j = 0; j < 256; j++)
	{ Q[j] = xorshift32(); }
}

unsigned int rng3()
{
	unsigned long long t, a = 809430660LL;
	static unsigned char i = 255;
	t = a * Q[++i] + c;
	c = ( t >> 32 );
	Q[i] = t;
	return t;
}

unsigned int rng3( int min, int max )
{
	unsigned int r = rng3();
	return r % ( max - min ) + min;
}

unsigned int rng3( int numberOfBits )
{
	unsigned int r = rng3();
	unsigned int mask = ( 1 << numberOfBits ) - 1;
	return r & mask;
}

// ---------------------------------------------------------------------
// Lehmer's rng.
// ---------------------------------------------------------------------

// https://en.wikipedia.org/wiki/Lehmer_random_number_generator


unsigned int seedLehmer = 684791;

unsigned int rngLehmer( )
{
	seedLehmer = ( (unsigned long)seedLehmer * 279470273u ) & 0xfffffffb;
	return seedLehmer;
}

unsigned int rngLehmer( int min, int max )
{
	unsigned int r = rngLehmer();
	return r % ( max - min ) + min;
}

unsigned int rngLehmer( int numberOfBits )
{
	unsigned int r = rngLehmer();
	unsigned int mask = (( 1 << numberOfBits ) - 1) << 16;
	unsigned int n = (r & mask) >> 16;
	return n;
}

// ---------------------------------------------------------------------
// Mersenne's Twister
// ---------------------------------------------------------------------

// https://en.wikipedia.org/wiki/Mersenne_Twister
// Quite likely a 'high quality' version.

void CheckDistribution( const char *name, unsigned int ( *rng )( int ) )
{
	const int numberOfIterations = 1000 * 1000;
	const int numberOfBits = 4;
	const int maximumNumber = 1 << numberOfBits;

	int distribution[maximumNumber] = {};

	for ( int j = 0; j < numberOfIterations; j++ )
	{
		unsigned int number = rng( numberOfBits );
		distribution[number]++;
	}

	printf( "The distribution of %s: \r\n", name );
	for ( int j = 0; j < maximumNumber; j++ )
	{ printf( " - Number: %i, occurences: %i \r\n", j, distribution[j] ); }
}
