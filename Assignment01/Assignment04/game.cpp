#include "precomp.h" // include (only) this in every .cpp file

#include "Particle.h"
#include "RandomGens.h"

// -----------------------------------------------------------
// Initialize the application
// -----------------------------------------------------------

Surface a = Surface( "assets/dotsBackground.png" );
Surface b = Surface( "assets/dotsBackground.png" );

Surface clear = Surface( "assets/dotsBackground.png" );

Surface *updateSurface = &a;
Surface *drawSurface = &b;

const int particlesPerFrame = 4;
const int numberOfParticles = 1024 * 4;
int currentParticleIndex = 0;
Particle ps[numberOfParticles];

void Game::Init()
{
}

// -----------------------------------------------------------
// Close down application
// -----------------------------------------------------------
void Game::Shutdown()
{
}

static Sprite rotatingGun( new Surface( "assets/aagun.tga" ), 36 );
static int frame = 0;

// -----------------------------------------------------------
// Main application tick function
// -----------------------------------------------------------
void Game::Tick( float deltaTime )
{
	// -----------------------------------------------------------
	// Switch around the surfaces and 'clear' the drawing one.
	// -----------------------------------------------------------

	Surface *tmp = updateSurface;
	updateSurface = drawSurface;
	drawSurface = tmp;

	clear.CopyTo( drawSurface, 0, 0 );

	// -----------------------------------------------------------
	// Spawn new particles.
	// todo: keep track of whether a particle is actually done.
	// -----------------------------------------------------------
	//if ( currentParticleIndex != 1 )
	{
		for ( int j = 0; j < particlesPerFrame; j++ )
		{
			const int index = ( currentParticleIndex + j ) % numberOfParticles;
			ps[index] = Particle( xorshift32( 0, 800 ), 0, 0xffffffff );
		}

		currentParticleIndex += particlesPerFrame;
	}

	// -----------------------------------------------------------
	// Update and draw all the particles. Take note: particles
	// that are not 'alive' are not updated nor drawn.
	// -----------------------------------------------------------
	for ( int k = 0; k < numberOfParticles; k++ )
	{ ps[k].Update( updateSurface ); }

	// for all particles, draw.
	for ( int k = 0; k < numberOfParticles; k++ )
	{ ps[k].Draw( drawSurface ); }

	// -----------------------------------------------------------
	// Copy it all to the actual surface?
	// -----------------------------------------------------------
	drawSurface->CopyTo( screen, 0, 0 );
}
