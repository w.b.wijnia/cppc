#include "precomp.h"
#include "Particle.h"


void Particle::Update( Surface *surface )
{
	// -----------------------------------------------------------
	// Check if we should be alive!
	// -----------------------------------------------------------
	const int maximumHeight = surface->GetHeight() - 20;
	if ( m_Y >= maximumHeight )
	{ m_Color == 0x00000000; }

	// check if we're 'alive'.
	if ( m_Color == 0x00000000 )
	{ return; }

	// -----------------------------------------------------------
	// Determine the behaviour of the pixel.
	// -----------------------------------------------------------

	const int offsetLeft = -1;
	const int offsetRight = 1;
	const int offsetDown = surface->GetWidth();

	// retrieve the pixel on our location.
	Pixel *p = ComputePointerToPixel( surface );

	// check below us: can we move down?
	if ( ( *( p + offsetDown ) ) == 0xff000000 )
	{
		m_Y++;
	}
	else
	{
		if ( ( m_Y & 1 ) == 1 )
		{
			// y coordinate is uneven. Check if we can move.
			if ( ( *( p + offsetLeft ) ) == 0xff000000 )
			{ m_X--; }
		}
		else
		{
			// y coordinate is even. Check if we can move.
			if ( ( *( p + offsetRight ) ) == 0xff000000 )
			{ m_X--; }
		}
	}
}

void Particle::Draw( Surface *surface )
{
	// check if we're 'alive'.
	if ( m_Color == 0x00000000 )
	{ return; }

	Pixel *p = ComputePointerToPixel( surface );
	( *p ) = m_Color;
}

Pixel *Particle::ComputePointerToPixel( Surface *surface )
{
	return surface->GetBuffer() + m_X + surface->GetWidth() * m_Y;
}
