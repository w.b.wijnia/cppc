
// -----------------------------------------------------------
// This snippet runs on http://quick-bench.com.
// -----------------------------------------------------------

#include <cstdio>
#include <cstdlib>

int numberOfRuns = 10000;

static void BenchmarkRand()
{
	for ( int j = 0; j < numberOfRuns; j++ )
	{
		rand();
	}
}

unsigned int seed = 123456;
unsigned int xorshift32()
{
	seed ^= seed << 13;
	seed ^= seed >> 17;
	seed ^= seed << 5;
	return seed;
}

void BenchmarkXorShift32()
{
	for (int j = 0; j < numberOfRuns; j++)
	{
		xorshift32();
	}
}