#pragma once

unsigned int xorshift32();
unsigned int xorshift32( int min, int max );
unsigned int xorshift32( int numberOfBits );

double whichmannHill();
unsigned int whichmannHill32( int min, int max );
unsigned int whichmannHill32( int numberOfBits );

unsigned int rng1();
unsigned int rng1( int min, int max );
unsigned int rng1( int numberOfBits );

unsigned int rng2();
unsigned int rng2( int min, int max );
unsigned int rng2( int numberOfBits );

void InitializeRNG3();
unsigned int rng3();
unsigned int rng3( int min, int max );
unsigned int rng3( int numberOfBits );

unsigned int rngLehmer();
unsigned int rngLehmer( int min, int max );
unsigned int rngLehmer( int numberOfBits );

void CheckDistribution( const char *name, unsigned int ( *rng )( int ) );