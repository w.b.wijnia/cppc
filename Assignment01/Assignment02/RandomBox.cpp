
#include "surface.h"
#include "RandomGens.h"

#include <cstdio>
#include <cstdlib>

using namespace Tmpl8;

Surface *myscreen;

unsigned int seed = 123456;

void RandomBox( int x1, int y1, int x2, int y2 )
{
	for ( int y = y1; y <= y2; y++ )
		for ( int x = x1; x <= x2; x++ )
		{
			myscreen->Plot( x, y, xorshift32() );
		}
}
