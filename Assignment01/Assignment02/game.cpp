#include "precomp.h" // include (only) this in every .cpp file

#include "RandomBox.h"
#include "RandomGens.h"

extern Surface *myscreen;

// -----------------------------------------------------------
// Initialize the application
// -----------------------------------------------------------
void Game::Init()
{
	myscreen = screen;

	const char* xorshift32Name = "xorshift32";
	CheckDistribution( xorshift32Name, xorshift32 );

	const char *wichmanHillName = "wichmanHill";
	CheckDistribution( wichmanHillName, whichmannHill32 );

	const char *rng1Name = "rng1";
	CheckDistribution( rng1Name, rng1 );

	const char *rng2Name = "rng2";
	CheckDistribution( rng2Name, rng2 );

	InitializeRNG3();
	const char *rng3Name = "rng3";
	CheckDistribution( rng3Name, rng3 );

	const char *rngLehmerName = "Lehmer";
	CheckDistribution( rngLehmerName, rngLehmer );
}

// -----------------------------------------------------------
// Close down application
// -----------------------------------------------------------
void Game::Shutdown()
{
}

static Sprite rotatingGun( new Surface( "assets/aagun.tga" ), 36 );
static int frame = 0;

// -----------------------------------------------------------
// Main application tick function
// -----------------------------------------------------------
void Game::Tick( float deltaTime )
{
	// clear the graphics window
	screen->Clear( 0 );
	// print something in the graphics window
	screen->Print( "hello world", 2, 2, 0xffffff );
	// draw a sprite
	rotatingGun.SetFrame( frame );
	rotatingGun.Draw( screen, 100, 100 );
	if ( ++frame == 36 ) frame = 0;

	RandomBox( 0, 0, 100, 100 );
}
