
#include "FixedPoint16.h"

// The definition of a float is:
//    0     0000 0000   000 0000 0000 0000 0000 0000
// | sign | exponent  | fraction				    |

// the number of bits per element is:
// sign:			1
// exponent (e):	8
// fraction:		23

// formula of a float is:	(-1)^(b31) * 2 ^(e -127) * (1 + sum (i = 1, 23) (b23 - i) * 2 ^ -i)
// sign:			(-1)^(b31)
// exponent (e):	2 ^(e -127)
// fraction:		(1 + sum (i = 1, 23) (b23 - i) * 2 ^ -i)

// the definition of our fixed point is:
//     0    0000 000   0000 0000
// | sign | upper    | lower     |

struct ConversionStruct
{
	union {
		unsigned int i;
		float f;
	};
};

void RetrieveInformationFromFloatingPoint(
	float f,
	unsigned int *sign,
	unsigned int *exponent,
	unsigned int *fraction )
{
	ConversionStruct cs;
	cs.f = f;

	( *sign ) = cs.i >> 31;
	( *exponent ) = ( cs.i << 1 ) >> 25;
	( *fraction ) = ( cs.i << 9 ) >> 9;
}

void RetrieveInformationFromFixedPoint(
	FixedPoint16 f,
	unsigned int *sign,
	unsigned int *exponent,
	unsigned int *fraction )
{
	( *sign ) = f >> 15;
	( *exponent ) = ( f << 1 ) >> 12;
	( *fraction ) = ( f << 5 ) >> 5;
}

FixedPoint16 FloatToFixed( float f )
{
	// retrieve all the information from the floating point.
	unsigned int sign, exponent, fraction;
	RetrieveInformationFromFloatingPoint( f, &sign, &exponent, &fraction );

	// make the information of the float
	// 'fit' in our 16 bit variable.

	sign = sign;			   // 1 bit to 1 bit
	exponent = (exponent << 4) >> 8;  // 8 bits to 4 bits.
	fraction = fraction >> 12; // 23 bits to 11 bits.

	// and finally construct our fixed point value.
	// and return it! :)

	FixedPoint16 fp = ( sign << 15 ) + ( exponent << 11 ) + fraction;
	return fp;
}

float FixedToFloat( FixedPoint16 f )
{
	// retrieve all the information from the fixed point.
	unsigned int sign, exponent, fraction;
	RetrieveInformationFromFixedPoint( f, &sign, &exponent, &fraction );

	// prepare the integer that represents the floating point.
	int floatingPoint = ( sign << 31 ) + ( exponent << 23 ) + fraction;

	// construct the union, return the float.
	ConversionStruct cs;
	cs.i = floatingPoint;
	return cs.f;
}