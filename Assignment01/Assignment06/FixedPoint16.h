#pragma once

// The FixedPoint16 is described as:
// | x    | xxx xxxx | xxxx xxxx |
// | sign | upper	 | lower	 |

// where the upper values represent	: 64, 32, 16, 8, 4, 2, 1
// and the lower values represent	: 1/2, 1/4, 1/8, 1/16, 1/32, 1/64, 1/128, 1/256
// take note that the point (komma) is fixed!

struct FixedPoint16
{
  public:
	// -----------------------------------------------------------
	// Constructs a fixed point number from the given float.
	// Take note: values must be between (-128 and 128) or
	// data loss will occur.
	// -----------------------------------------------------------
	FixedPoint16( float f );

	// -----------------------------------------------------------
	// Constructs a fixed point number from the given integer.
	// Take note: values must be between (-128 and 128) or
	// data loss will occur.
	// -----------------------------------------------------------
	FixedPoint16( int i );

	// -----------------------------------------------------------
	// Adds the given fixed point number to this fixed
	// point number.
	// -----------------------------------------------------------
	FixedPoint16 operator+( FixedPoint16 const &rhs );

	// -----------------------------------------------------------
	// Subtracts the given fixed point number from this
	// fixed point number.
	// -----------------------------------------------------------
	FixedPoint16 operator-( FixedPoint16 const &rhs );

	// todo :(
	//FixedPoint16 operator *( FixedPoint16 const &rhs );
	//FixedPoint16 operator /( FixedPoint16 const &rhs );

	// -----------------------------------------------------------
	// Computes the float representation.
	// -----------------------------------------------------------
	float ToFloat();

  private:
	// -----------------------------------------------------------
	// Represents our fixed point number.
	// -----------------------------------------------------------
	__int16 data;

	// -----------------------------------------------------------
	// Directly copies the given short into this fixed point number.
	// -----------------------------------------------------------
	FixedPoint16( __int16 i );

	// -----------------------------------------------------------
	// Directly adds the given short to this fixed point number.
	// -----------------------------------------------------------
	FixedPoint16 operator+( __int16 const &i );
};