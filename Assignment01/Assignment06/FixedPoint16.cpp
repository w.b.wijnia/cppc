
#include "FixedPoint16.h"
#include <stdio.h>

// -----------------------------------------------------------
// The number of values (sign excluded), and the maximum /
// minimum value a fixed point number can hold.
// -----------------------------------------------------------

unsigned int const numberOfValues = 15;
float const maxValue = 128.0f;
float const minValue = -128.0f;

// -----------------------------------------------------------
// Represents all the values of the according bits.
// -----------------------------------------------------------

const float values[numberOfValues] = {
	64.0f / 1.0f,
	32.0f / 1.0f,
	16.0f / 1.0f,
	8.0f / 1.0f,
	4.0f / 1.0f,
	2.0f / 1.0f,
	1.0f / 1.0f,

	/* point / comma */

	1.0f / 2.0f,
	1.0f / 4.0f,
	1.0f / 8.0f,
	1.0f / 16.0f,
	1.0f / 32.0f,
	1.0f / 64.0f,
	1.0f / 128.0f,
};

// -----------------------------------------------------------
// Allows us to easily retrieve the sign from the float
// without any branching.
// -----------------------------------------------------------
struct ConversionStruct
{
	union {
		int i;
		float f;
	};
};

FixedPoint16::FixedPoint16( float f )
{
	// defensive programming :).
	if ( f > maxValue || f < minValue )
	{ printf( "The number %f is not between (%f, %f). Loss of data will happen. \r\n", f, maxValue, minValue ); }

	// -----------------------------------------------------------
	// Retrieve the sign.
	// -----------------------------------------------------------

	ConversionStruct cs;
	cs.f = f;

	this->data = ( cs.i >> 31 ) << 15;

	// -----------------------------------------------------------
	// Compute the number.
	// -----------------------------------------------------------

	for ( int j = 0; j < numberOfValues; j++ )
	{
		float value = values[j];
		if ( f > value )
		{
			f -= value;
			this->data = this->data + ( 1 << ( numberOfValues - 1 - j ) );
		}
	}
}

FixedPoint16::FixedPoint16( __int16 i )
{
	( *this ).data = i;
}

FixedPoint16::FixedPoint16( int i )
{
	// defensive programming :).
	if ( i > maxValue || i < minValue )
	{ printf( "The number %i is not between (%f, %f). Loss of data will happen. \r\n", i, maxValue, minValue ); }

	// -----------------------------------------------------------
	// Retrieve the sign.
	// -----------------------------------------------------------

	// determine the sign.
	( *this ).data = ( ( i >> 31 ) << 15 );

	// -----------------------------------------------------------
	// Compute the number.
	// -----------------------------------------------------------

	for ( int j = 0; j < numberOfValues - 8; j++ )
	{
		float value = values[j];
		if ( i > value )
		{
			i -= (int)value;
			( *this ).data = ( *this ).data + ( 1 << ( numberOfValues - 1 - j ) );
		}
	}
}

float FixedPoint16::ToFloat()
{
	float f = 0;

	// -----------------------------------------------------------
	// Compute the number.
	// -----------------------------------------------------------

	for ( int j = 0; j < numberOfValues; j++ )
	{
		int bit = 1 << ( numberOfValues - 1 - j );
		if ( ( *this ).data & bit )
		{ f += values[j]; }
	}

	// -----------------------------------------------------------
	// Retrieve the sign.
	// -----------------------------------------------------------

	int sign = 1 + ( ( ( *this ).data >> 15 ) << 31 );
	return f * sign;
}

FixedPoint16 FixedPoint16::operator+( __int16 const &rhs )
{ return FixedPoint16( __int16( this->data + rhs ) ); }

FixedPoint16 FixedPoint16::operator+( FixedPoint16 const &rhs )
{ return FixedPoint16( __int16(this->data + rhs.data) ); }

FixedPoint16 FixedPoint16::operator-( FixedPoint16 const &rhs )
{ return FixedPoint16( __int16( this->data - rhs.data ) ); }