

#include "precomp.h" // include (only) this in every .cpp file

#define CUTE_SOUND_IMPLEMENTATION
#define CUTE_SOUND_PITCH_PLUGIN_IMPLEMENTATION

#include "FixedPoint16.h"
#include "cute_sound.h"
#include "cute_sound_pitch_plugin.h"

cs_context_t *ctx;
cs_loaded_sound_t jump_audio;
cs_playing_sound_t jump_instance;
cs_plugin_id_t pitch_plugin_id;
cs_plugin_interface_t plugin;

// -----------------------------------------------------------
// Initialize the application
// -----------------------------------------------------------
void Game::Init()
{

	// (hwnd, frequency, latency, seconds, N);
	ctx = cs_make_context( GetConsoleWindow(), 44100, 8192, 0, NULL );

	plugin = csp_get_pitch_plugin();
	pitch_plugin_id = cs_add_plugin( ctx, &plugin );

	jump_audio = cs_load_wav( "assets/piano2.wav" );
	jump_instance = cs_make_playing_sound( &jump_audio );
	cs_insert_sound( ctx, &jump_instance );

	// yeeeehhh!!
	FixedPoint16 fp1 = FixedPoint16( 5.5f );
	FixedPoint16 fp2 = FixedPoint16( 2.5f );
	FixedPoint16 fpAdd = fp1 + fp2;
	FixedPoint16 fpSubtract = fp1 - fp2;
	printf( "Base values:: %f, %f \r\n", fp1.ToFloat(), fp2.ToFloat() );
	printf( "Addition: %f \r\n", fpAdd.ToFloat() );
	printf( "Subtraction: %f \r\n", fpSubtract.ToFloat() );

	int w = 0;
}

// -----------------------------------------------------------
// Close down application
// -----------------------------------------------------------
void Game::Shutdown()
{
}

static Sprite rotatingGun( new Surface( "assets/aagun.tga" ), 36 );
static int frame = 0;

unsigned int i = 123456789;

unsigned int GenerateRandomNumber()
{
	// any of the following numbers work for this rng
	// 124323421 343767343 351919153 377000773 704606407 725595527 911919119 999727999
	i = i * 124323421 + 1;
	return i;
}

unsigned int x = 88172645463325252LL;

unsigned int xor32()
{
	x = x ^ ( x << 13 );
	x = x ^ ( x >> 7 );
	return ( x = x ^ ( x << 17 ) );
}

// -----------------------------------------------------------
// Main application tick function
// -----------------------------------------------------------

float pan = 0.5f;
float pitch = 1.0f;
void Game::Tick( float deltaTime )
{

	if ( GetAsyncKeyState( 32 ) ) // space
	{
		jump_instance.sample_index = 0;
		pan = 0.5f;
		pitch = 1.0f;
	}

	if ( GetAsyncKeyState( VK_LEFT ) )
	{ pan = clamp( pan - 0.01f, 0.0f, 1.0f ); }

	if ( GetAsyncKeyState( VK_RIGHT ) )
	{ pan = clamp( pan + 0.01f, 0.0f, 1.0f ); }

	if ( GetAsyncKeyState( VK_DOWN ) )
	{ pitch = clamp( pitch - 0.001f, 0.0f, 4.0f ); }

	if ( GetAsyncKeyState( VK_UP ) )
	{ pitch = clamp( pitch + 0.001f, 0.0f, 4.0f ); }

	cs_set_pan( &jump_instance, pan );
	csp_set_pitch( &jump_instance, pitch, pitch_plugin_id );

	// play sound!
	cs_mix( ctx );

	int x = xor32() % screen->GetWidth();
	int y = xor32() % screen->GetHeight();

	screen->Plot( x, y, 0xffffffff );
}
