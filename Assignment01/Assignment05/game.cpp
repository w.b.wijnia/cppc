#include "precomp.h" // include (only) this in every .cpp file

Surface original = Surface( "assets/starwars.png" );
Surface image = Surface( "assets/starwars.png" );

Pixel *GetSurfacePixel( Surface *surface, int x, int y )
{
	return surface->GetBuffer() + x + y * surface->GetWidth();
}

void HalfContrastImage( Surface *surface )
{
	for ( int y = 0; y < image.GetHeight(); y++ )
	{
		Pixel *p = GetSurfacePixel( surface, 0, y );

		for ( int x = 0; x<image.GetWidth()>> 1; x++ )
		{
			Pixel *px = p + x;
			unsigned int pi = ( *px );

			unsigned int r = ( ( pi << 8 ) >> 24 ) * 128 / 256;
			unsigned int g = ( ( pi << 16 ) >> 24 ) * 128 / 256;
			unsigned int b = ( ( pi << 24 ) >> 24 ) * 128 / 256;

			( *px ) = 0xff000000 + ( r << 16 ) + ( g << 8 ) + b;
		}
	}
}

void FadeImage( Surface *surface, int fadeValue )
{
	for ( int y = 0; y < image.GetHeight(); y++ )
	{
		Pixel *p = GetSurfacePixel( surface, 0, y );

		for ( int x = 0; x<image.GetWidth(); x++ )
		{
			Pixel *px = p + x;
			unsigned int pi = ( *px );

			unsigned int r = ( ( pi << 8 ) >> 24 ) * fadeValue / 256;
			unsigned int g = ( ( pi << 16 ) >> 24 ) * fadeValue / 256;
			unsigned int b = ( ( pi << 24 ) >> 24 ) * fadeValue / 256;

			( *px ) = 0xff000000 + ( r << 16 ) + ( g << 8 ) + b;
		}
	}
}

// -----------------------------------------------------------
// Initialize the application
// -----------------------------------------------------------
void Game::Init()
{
}

// -----------------------------------------------------------
// Close down application
// -----------------------------------------------------------
void Game::Shutdown()
{
}

// -----------------------------------------------------------
// Main application tick function
// -----------------------------------------------------------

int fadeValue = 0;

void Game::Tick( float deltaTime )
{
	fadeValue = ( fadeValue + 1 ) % 256;
	original.CopyTo( &image, 0, 0 );
	FadeImage( &image, fadeValue );

	image.CopyTo( screen, 0, 0 );
}
