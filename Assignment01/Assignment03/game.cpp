#include "precomp.h" // include (only) this in every .cpp file

// -----------------------------------------------------------
// Initialize the application
// -----------------------------------------------------------
void Game::Init()
{
}

// -----------------------------------------------------------
// Close down application
// -----------------------------------------------------------
void Game::Shutdown()
{
}

// -----------------------------------------------------------
// Main application tick function
// -----------------------------------------------------------

Surface image( "assets/ball.png" );

void TestOne()
{
	char *pos;
	//char t[256] = "test    "; // this one works
	char t[256] = "        "; // this one crashes, why?

	// we do not stop when we reach the end of t.
	// hence, we start changing memory outside of t
	// (going into the stack, downwards) until we find
	// something that is > 33.

	// pos = t + strlen( t ) - 1;
	// while ( *pos < 33 )
	// {
	//	 *pos-- = 0;
	// }

	// we can solve it by keeping track of the number of characters
	// we've already eliminated.

	int maxCharacters = strlen( t );
	pos = t + strlen( t ) - 1;
	while ( maxCharacters > 0 && * pos < 33 )
	{
		maxCharacters--;
		*pos-- = 0;
	}

	printf( "spaces removed: [%s]\n", t );
}

void TestTwo()
{

	// Visual Studio cheated this one :).
	// 'char values are always in range of -128 to 127.'

	// e.g., the loop continues into eternity because it will
	// overflow from 127 to -128, never reaching 256.

	// changing everything to int's solves the issue.

	// fill an array with values
	int t[256];
	for ( int i = 0; i < 256; i++ ) 
	{ 
		t[i] = i; 
	}
	// compute the sum of the values in the array
	int total = 0;
	for ( int i = 0; i < 256; i++ )
	{
		total += t[i];
	}
	printf( "total: %i\n", total );
}

void Game::Tick( float deltaTime )
{
	// clear the graphics window
	screen->Clear( 0 );

	// draw a grid
	for ( int x = 15; x < 800; x += 16 )
	{
		for ( int y = 6; y < 512; y += 12 )
		{
			Pixel p = image.GetBuffer()[x / 16 + ( y / 12 ) * 50];
			int red = p & 0xff0000;
			int green = p & 0x00ff00;
			int blue = p & 0x0000ff;
			screen->Bar( x, y, x + 12, y + 2, red );
			screen->Bar( x, y + 4, x + 12, y + 6, green );
			screen->Bar( x, y + 8, x + 12, y + 10, blue );
		}
	}

	TestOne();
	TestTwo();
}
