#pragma once

namespace Tmpl8 {

class Game
{
public:
	void SetTarget( Surface* surface ) { screen = surface; }
	void Init();
	void Shutdown();
	void Tick( float deltaTime );
	void MouseUp( int button ) { /* implement if you want to detect mouse button presses */ }
	void MouseDown( int button ) { /* implement if you want to detect mouse button presses */ }
	void MouseMove( int x, int y ) { /* implement if you want to detect mouse movement */ }
	void KeyUp( int key ) { /* implement if you want to handle keys */ }
	void KeyDown( int key ) { /* implement if you want to handle keys */ }
	void DrawSpriteBox( Sprite *sprite, int frames, int x, int y, int w, int h );
	void Benchmark( Sprite *sprite, int frames, vec2 origin, float radius, int numberOfSprites );
  private:
	Surface* screen;
};

}; // namespace Tmpl8