#include "precomp.h" // include (only) this in every .cpp file

// -----------------------------------------------------------
// Initialize the application
// -----------------------------------------------------------
void Game::Init()
{
}

// -----------------------------------------------------------
// Close down application
// -----------------------------------------------------------
void Game::Shutdown()
{
}

static Sprite rotatingGun( new Surface( "assets/aagun.tga" ), 36 );
static int frame = 0;
static int numberOfSprites = 100;

// -----------------------------------------------------------
// Main application tick function
// -----------------------------------------------------------
void Game::Tick( float deltaTime )
{
	// clear the graphics window
	screen->Clear( 0 );
	// print something in the graphics window
	//screen->Print( "hello world", screen->GetWidth() - 20, 2, 0xffffff );
	// print something to the text window
	printf( "this goes to the console window.\n" );
	// draw a sprite

	Benchmark( &rotatingGun, 36, vec2( 400, 400 ), 200, numberOfSprites );
	numberOfSprites += 10;
}

// -----------------------------------------------------------
// Used for benchmarking the current machine.
// -----------------------------------------------------------
void Game::Benchmark( Sprite *sprite, int frames, vec2 origin, float radius, int numberOfSprites )
{
	char output[50];
	const char text[] = "Number of sprites: ";
	sprintf( output, "%s%i", text, numberOfSprites );
	screen->Print( output, 10, 10, 0xffffff );

	for ( int j = 0; j < numberOfSprites; j++ )
	{
		vec2 offset = origin + vec2( ( rand() % 10 ) - 5, ( rand() % 10 ) - 5 ).normalized() * radius;
		int frameNumber = rand() % frames;

		sprite->SetFrame( frameNumber );
		sprite->Draw( screen, (int)offset.x, (int)offset.y );
	}
}

// -----------------------------------------------------------
// Draws the given sprite over the edges of the box.
// -----------------------------------------------------------
void Game::DrawSpriteBox( Sprite *sprite, int frames, int x, int y, int w, int h )
{
	int frame = 0;
	int offset = 10;

	for ( int ly = y; ly <= y + h; ly += offset )
	{
		if ( ++frame == frames ) { frame = 0; }
		sprite->SetFrame( frame );
		sprite->Draw( screen, x, ly );
	}

	for ( int lx = x; lx <= x + w; lx += offset )
	{
		if ( ++frame == frames ) { frame = 0; }
		sprite->SetFrame( frame );
		sprite->Draw( screen, lx, y );
	}

	for ( int ly = y; ly <= y + h; ly += offset )
	{
		if ( ++frame == frames ) { frame = 0; }
		sprite->SetFrame( frame );
		sprite->Draw( screen, x + w, ly );
	}

	for ( int lx = x; lx <= x + w; lx += offset )
	{
		if ( ++frame == frames ) { frame = 0; }
		sprite->SetFrame( frame );
		sprite->Draw( screen, lx, y + h );
	}
}
